'use strict'
const request = require('supertest')
const server = require('../app.js')
const chai = require('chai')
const chaiHttp = require('chai-http')

chai.should()
chai.use(chaiHttp)

describe('Events API', () => {

    describe('GET /info', () => {
        it('It should send jsau-apiserver ', (done) => {
            chai.request(server)
                .get('/info')
                .end((err, res) => {
                    if (!err) {
                        res.text.should.be.eq('jsau-apiserver-1.0.0')
                        done()
                    }
                })
        })

    })

    describe('GET /events', () => {
        it('It should GET all events', (done) => {
            chai.request(server)
                .get('/events')
                .end((err, res) => {
                    if (!err) {
                        res.should.have.status(200)
                        res.body.should.be.a('array')
                        done()
                    }
                })
        })
        it('It should NOT GET all events', (done) => {
            chai.request(server)
                .get('/event')
                .end((err, res) => {
                    if (!err) {
                        res.should.have.status(404)
                        done()
                    }
                })
        })

    })

    describe('GET /events', () => {
        it('responds with json', (done) => {
            request(server)
                .get('/events')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200, done)
        })
    })

    describe('GET /events/:id', () => {
        it('It should GET an event by ID', (done) => {
            const eventId = 1
            chai.request(server)
                .get('/events/' + eventId)
                .end((err, res) => {
                    if (!err) {
                        res.should.have.status(200)
                        res.body.should.be.a('object')
                        res.body.should.have.property('id')
                        res.body.should.have.property('title')
                        res.body.should.have.property('description')
                        res.body.should.have.property('place')
                        res.body.should.have.property('date')
                        res.body.should.have.property('begin')
                        res.body.should.have.property('end')
                        res.body.should.have.property('id').eq(1)
                        done()
                    }
                })
        })

        it('It should NOT GET an event by ID', (done) => {
            const eventId = 100
            chai.request(server)
                .get('/events/' + eventId)
                .end((err, res) => {
                    if (!err) {
                        res.should.have.status(404)
                        res.text.should.be.eq('The event with the provided ID does not exist.')
                        done()
                    }
                })
        })

    })

    describe('POST /events', () => {
        it('It should POST a new event', (done) => {
            chai.request(server)
                .post('/events')
                .end((err, res) => {
                    if (!err) {
                        res.should.have.status(201)
                        res.body.should.be.a('object')
                        done()
                    }
                })
        })
    })

    describe('POST /events', () => {
        it('responds with json', (done) => {
            request(server)
                .post('/events')
                .send({title: 'titre'})
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(201)
                .end((err, res) => {
                    if (err) {
                        return done(err)
                    }
                    return done()
                })
        })
    })

    describe('PUT /events/:id', () => {
        it('It should PUT an  existing event', (done) => {
            const eventId = 1
            chai.request(server)
                .put('/events/' + eventId)
                .end((err, res) => {
                    if (!err) {
                        res.should.have.status(200)
                        res.body.should.be.a('object')
                        done()
                    }
                })
        })

        it('It should NOT put an event by ID', (done) => {
            const eventId = 10
            chai.request(server)
                .put('/events/' + eventId)
                .end((err, res) => {
                    if (!err) {
                        res.should.have.status(400)
                        res.text.should.be.eq('event does not exist')
                        done()
                    }
                })
        })

    })
    describe('DELETTE /events/:id', () => {
        it('It should DELETTE an  existing event', (done) => {
            const eventId = 2
            chai.request(server)
                .delete('/events/' + eventId)
                .end((err, res) => {
                    if (!err) {
                        res.should.have.status(200)
                        res.text.should.be.eq('Event removed successfully')
                        done()
                    }
                })
        })

        it('It should NOT DELETTE an event by ID', (done) => {
            const eventId = 10
            chai.request(server)
                .delete('/events/' + eventId)
                .end((err, res) => {
                    if (!err) {
                        res.should.have.status(404)
                        res.text.should.be.eq('event does not exist')
                        done()
                    }
                })
        })

    })

})
