'use strict'
const express = require('express')
const app = express()
const morgan = require('morgan')
const port = 8081
app.use(morgan('dev'))
app.use(express.json())
const bunyan = require('bunyan')
const log = bunyan.createLogger({name:String})
const fs = require('fs')
const {readFile} = require('fs').promises
const eventsPath = 'data/events.json'

// method info
app.use('/info', (req, res) => {
    res.send('jsau-apiserver-1.0.0')
})

app.get('/events', (req, res) => {
    // async-callback
    fs.readFile(eventsPath, 'utf8', (err, data) => {
        if (err) {
            log.info('erreur')
        } else {
            try {
                const obj = JSON.parse(data)
                res.json(obj)
            } catch {
                log.info('erreur')
            }
        }
    })
})

app.post('/events', (req, res) => {
    getEventsData().then((obj) => {
        const idEvent = Date.now()
        const newEvents = {
            id:idEvent,
            ...req.body
        }
        obj.push(newEvents)
        saveEventsData(obj)
        res.status(201).send(newEvents)
    })
})

// async-promise-async-await
app.get('/events/:id', async(req, res) => {
    try {
        await getEventsData().then((obj) => {
            const id = parseInt(req.params.id)
            const event = obj.find(ev => ev.id === id)
            if (!event) {
                return res.status(404).send('The event with the provided ID does not exist.')
            }
            res.status(200).json(event)
        })
    } catch {
        log.info('erreur')
    }
})

app.put('/events/:id', (req, res) => {
    const idEv = parseInt(req.params.id)
    getEventsData().then((obj) => {
        const allEvents = obj
        const newEvents = {
            id:idEv,
            ...req.body
        }
        //filter the event to remove it
        const findEvent = allEvents.find(idEvent => idEvent.id === idEv)
        if (!findEvent) {
            return res.status(400).send('event does not exist')
        }
        const updateEvents = allEvents.filter(idEvent => idEvent.id !== idEv)
        updateEvents.push(newEvents)
        //save the filtered data
        saveEventsData(updateEvents)
        res.status(200).send(newEvents)
    })
})

app.delete('/events/:id', (req, res) => {
    getEventsData().then((obj) => {
        const id = parseInt(req.params.id)
        const allEvents = obj
        //filter the event to remove it
        const filterEvents = allEvents.filter(idEvent => idEvent.id !== id)
        if (allEvents.length === filterEvents.length) {
            return res.status(404).send('event does not exist')
        }
        //save the filtered data
        saveEventsData(filterEvents)
        res.status(200).send('Event removed successfully')
    })

})

const saveEventsData = (data) => {
    const stringifyData = JSON.stringify(data)
    fs.writeFile(eventsPath, stringifyData, (err) => {
        if (err) {
            throw err
        }
        log.info('The file has been saved!')
    })
}

// async-promise-then
const getEventsData = () => {
    return readFile(eventsPath, 'utf8')
        .then(JSON.parse)
        .then((obj) => {
            //log.info(obj)
            return obj
        })
        .catch((err) => {
            if (err instanceof SyntaxError) {
                log.error('Invalid JSON in file', err)
            } else {
                log.error('Unknown error', err)
            }
        })
}

app.listen(port, () => {
    log.info(`App listening at http://localhost:${port}`)
})

module.exports = app
